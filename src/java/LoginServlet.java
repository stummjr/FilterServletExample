import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns = {"/Login"})
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("   <head>");
            out.println("       <title>Servlet LoginServlet</title>");            
            out.println("   </head>");
            out.println("   <body>");
            out.println("       <h1>Servlet Login at " + request.getContextPath() + "</h1>");
            out.println("       <form action=\""+ request.getContextPath() +"/Login\" method=\"post\">");
            out.println("           <input type=\"text\" name=\"username\" />");
            out.println("           <input type=\"password\" name=\"password\" />");
            out.println("           <input type=\"submit\" name=\"submit\" value=\"Login\"/>");
            out.println("       </form>");
            out.println("   </body>");
            out.println("</html>");
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            // para ilustrar, nome de usuário e senha fixos no código
            if (request.getParameter("username").equals("root") && request.getParameter("password").equals("root")) {
                // armazena o nome do usuário na sessão, para depois poder verificar que ele está logado
                request.getSession().setAttribute("username", "root");
                response.sendRedirect(request.getContextPath() + "/Main");
            } else {
                response.sendRedirect(request.getContextPath() + "/Login");
            }
        }
    }
}
